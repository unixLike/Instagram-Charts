import json


def get(key):
    with open("config.json", "r") as conf:
        j = json.loads(conf.read())
        for k in j:
            if k == key:
                return j[key]

        raise KeyError