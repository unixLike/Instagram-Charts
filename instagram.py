import requests
import json
from bs4 import BeautifulSoup
import config
from time import sleep
from socket import gaierror


class RequestError(Exception):
    pass


class Instagram:
    def __init__(self, username):
        self.json = None
        self.username = username
        self.page_url = config.get("URL").format(self.username)

    def update_data(self):
        self.json = self.__json_from_html()

    def __download_page(self):
        success = False

        while not success:
            try:
                html = requests.get(self.page_url, headers=config.get("headers"))
            except (requests.exceptions.ConnectionError, gaierror):
                sleep(1.25)
                success = False
            else:
                if html.status_code != 200:
                    raise RequestError(f"Error while requesting {self.page_url}, status code: {html.status_code}")
                else:
                    return html.text

    def __json_from_html(self):
        parser = BeautifulSoup(self.__download_page(), "html.parser")
        script = str(parser.find_all("script", {"type": "text/javascript"})[3])
        script = script.replace('<script type="text/javascript">window._sharedData = ', "", 1)
        script = script[:script.rfind(";")]
        return json.loads(script)

    def get_posts_count(self):
        posts_count = \
            self.json["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["count"]
        return int(posts_count)

    def get_followers_count(self):
        followers_count = self.json["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_followed_by"]["count"]
        return int(followers_count)

    def get_following_count(self):
        following_count = self.json["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_follow"]["count"]
        return int(following_count)