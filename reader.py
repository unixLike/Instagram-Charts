import config
import matplotlib.pyplot as plt
from datetime import datetime
import json


def header_reader():
    with open(config.get("data_filename"), "r") as header:
        lines = header.readlines()

    pages = json.loads(lines[0][lines[0].find("["):].replace("'", '"'))
    sleep_time = lines[1][lines[1].find(":")+1:]

    return {
        "pages": pages,
        "sleep_time": sleep_time,
    }


def reader(selected_username):
    timestamps = []
    posts = []
    followers = []
    following = []

    with open(config.get("data_filename"), "r") as r:
        lines_tmp = r.readlines()
        lines = [lines_tmp[x] for x in range(4, len(lines_tmp))]
        del lines_tmp

    for line in lines:
        line = line.split()
        username = line[0][:line[0].find(",")]

        # I know, it's fucking unreadable
        if username == selected_username:
            timestamps.append(int(line[1][:line[1].find(":")]))
            posts.append(int(line[2][:line[2].find(",")]))
            followers.append(int(line[3][:line[3].find(",")]))
            following.append(int(line[4]))

    first_post = posts[0]
    for this in range(len(posts)):
        posts[this] -= first_post

    first_followers = followers[0]
    for this in range(len(followers)):
        followers[this] -= first_followers

    first_following = following[0]
    for this in range(len(following)):
        following[this] -= first_following

    return {
        "dates": [datetime.fromtimestamp(timestamp) for timestamp in timestamps],
        "posts": posts,
        "followers": followers,
        "following": following,
        "before_posts": first_post,
        "before_followers": first_followers,
        "before_following": first_following
    }


def str_to_capital(string):
    string = list(string)
    string[0] = chr(ord(string[0])-32)
    return "".join(string)


def main():
    headers = header_reader()
    profiles = headers["pages"]
    readeds = [reader(profile) for profile in profiles]
    selected = "followers"

    for readed in readeds:
        plt.plot(readed["dates"], readed[selected])

    plt.gcf().autofmt_xdate()
    selecteds = [count[f"before_{selected}"] for count in readeds]

    plt.xlabel("Time")
    plt.ylabel(str_to_capital(selected))
    plt.title(f"Increase of {selected}")
    plt.legend([profile + ": " + str(sel) + " " + selected for sel, profile in zip(selecteds, profiles)])
    plt.show()