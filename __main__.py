from time import sleep
from time import time as _time
import config
from instagram import Instagram
import reader


# Get rounded timestamp as an int
def time(): return round(_time())


def write_headers(p):
    with open(config.get("data_filename"), "a") as headers:
        s = f"pages: {[page.username for page in p]}\n" \
            f"sleep_time: {config.get('sleep_time')}\n\n"

        headers.write(s)


# Write time to text file
def write_time(page, downloaded_time):
    username = page.username
    posts = page.get_posts_count()
    followers = page.get_followers_count()
    following = page.get_following_count()

    with open(config.get("data_filename"), "a") as data:
        data.write(f"{username}, {downloaded_time}: {posts}, {followers}, {following}\n")
        print(downloaded_time, username, posts, followers, following, sep="\t")


def main():
    while True:
        t = time()

        for page in pages:
            page.update_data()
            print(f"Updated data of {page.username}")
            sleep(config.get("wait_time"))

        for page in pages:
            write_time(page, t)

        sleep(config.get("sleep_time"))
        print()


if __name__ == "__main__":
    if config.get("read"):
        reader.main()

    else:
        pages = [Instagram(username=page) for page in config.get("track-pages")]

        # First cleanup data file
        with open(config.get("data_filename"), "w") as clean:
            clean.write("")
            clean.close()

        # Then write header to it
        write_headers(pages)

        # And then launch the main script
        main()